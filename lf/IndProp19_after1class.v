(** * IndProp19: Inductively Defined Propositions *)

(** Adapted from _Software Foundations_  for SemProg@FAU 2013--2019 *)

Set Warnings "-notation-overridden,-parsing".
From Coq Require Import ssreflect ssrbool ssrfun.

Require Import PeanoNat. (* for basic properties of [nat] functions! *)
Import Nat.



(* ################################################################# *)
(** * Inductively Defined Propositions *)

(** Let us now have a look at _inductive definitions_ of propositions.

    We have seen two ways of stating evenness of a number [n]: (1) [even =
    true], and (2) [exists k, n = double k]

    But we could also establish evenness from the following rules

      - Rule [ev_0]: The number [0] is even.
      - Rule [ev_SS]: If [n] is even, then [S (S n)] is even.

    Let us try to show [4] is even using these rules:

      - By [ev_SS], it suffices to show that [2] is even.
      - This is guaranteed by [ev_SS] again, if we can show that [0] is even.
      - [0] is even by [ev_0]. *)

(**  Such definitions are often presented using inference rules, just like with logical connectives themselves: 

                              ------------                        (ev_0)
                                 ev 0

                                  ev n
                             --------------                      (ev_SS)
                              ev (S (S n))

*)

(** We can use a proof tree to depict why [4] is even: 

                ------  (ev_0)
                 ev 0
                ------ (ev_SS)
                 ev 2
                ------ (ev_SS)
                 ev 4
*)

(** Putting all of this together, we can translate the definition of
    evenness into a formal Coq definition using an [Inductive]
    declaration, where each constructor corresponds to an inference
    rule: *)

Inductive ev : nat -> Prop :=
| ev_0 : ev 0
| ev_SS : forall n : nat, ev n -> ev (S (S n)).

(**
    Had we tried to bring
    [nat] to the left in defining [ev], we would have seen an error:
    *)

Fail Inductive wrong_ev (n : nat) : Prop :=
| wrong_ev_0 : wrong_ev 0
| wrong_ev_SS : forall n, wrong_ev n -> wrong_ev (S (S n)).
(* ===> Error: A parameter of an inductive type n is not
        allowed to be used as a bound variable in the type
        of its constructor. *)

(** ("Parameter" here is Coq jargon for an argument on the left of the
    colon in an [Inductive] definition; "index" is used to refer to
    arguments on the right of the colon.) *)

(** We can think of the definition of [ev] as defining a Coq property
    [ev : nat -> Prop], together with theorems [ev_0 : ev 0] and
    [ev_SS : forall n, ev n -> ev (S (S n))].  Such "constructor
    theorems" have the same status as proven theorems.  In particular,
    we can use Coq's [apply] tactic with the rule names to prove [ev]
    for particular numbers... *)

Theorem ev_4 : ev 4.
Proof. apply ev_SS. apply ev_SS. apply ev_0. Qed.

(** ... or we can use function application syntax: *)

Theorem ev_4' : ev 4.
Proof. apply (ev_SS 2 (ev_SS 0 ev_0)). Qed.

Theorem ev_4'' : ev 4.
Proof. do! apply ev_SS. by apply ev_0. Qed.

(** We can also prove theorems that have hypotheses involving [ev]. *)

Theorem ev_plus4 : forall n, ev n -> ev (4 + n).
Proof.
  move => n Hn.
  apply ev_SS. apply ev_SS. apply Hn.
Qed.

(* ================================================================= *)
(** ** Using Evidence in Proofs *)

(** We can also _reason about_ evidence.

    [ev_0] and [ev_SS] are the _only_ valid ways to build evidene of evenness
    (in the sense of [ev]). 

    In other words, if someone gives us evidence [E] for the assertion
    [ev n], then we know that [E] must have one of two shapes:

      - [E] is [ev_0] (and [n] is [O]), or
      - [E] is [ev_SS n' E'] (and [n] is [S (S n')], where [E'] is
        evidence for [ev n']). *)

(** So we should be able to argue by _induction_ and _case analysis_ on
    evidence. *)

(** Subtracting two from an even number yields another even number. Let's
    state it in terms of [even] from the Nat standard library: *)


Theorem even_minus2: forall n,
  even n = true -> even (pred (pred n)) = true.
Proof.
  by move => [ | [ | n' ] ].
Qed.


(** We can state the same claim in terms of [ev], but this quickly
    leads us to an obstacle: Since [ev] is defined inductively --
    rather than as a function -- Coq doesn't know how to simplify a
    goal involving [ev n] after case analysis on [n]... *)

Theorem ev_minus2: forall n,
  ev n -> ev (pred (pred n)).
Proof.
  move => [ | [ | n' ] ].
  - (* n = 0 *) move => ?; by apply ev_0.
  - (* n = 1 *) move => ?; by apply ev_0.
  -  (* n = n' + 2 *) move => /= hip.  (* what now though? *)
     case: hip.
Abort.

(** Solution: perform case analysis on the _evidence_:

    - If it is fo the form [ev_0], then [n = 0], and we need to show [ev (predn
      (predn 0))] (easy).

    - Otherwise, it must be [ev_SS n' E'], where [n = (S (S n'))] and [E'] is
      evidence for [ev n']. We must show [ev (predn (predn (S (S n'))))] (easy). *)

(** Here is how it's done formally: *)

Theorem ev_minus2 : forall n,
  ev n -> ev (pred (pred n)).
Proof.
  move => n [| n' E'].
  - (* E = ev_0 *) by apply ev_0.
  - (* E = ev_SS n' E' *) by  apply E'.  Qed.

(** There are, however, more problematic  instances. Here's one: *)

Theorem evSS_ev : forall n,
  ev (S (S n)) -> ev n.
Proof.
  move => n  [ | n' E'].
 (* E = ev_0. *)
    (* We must prove that [n] is even from no assumptions! *)
Abort.

(** What happened?

    - [case] replaced all occurrences of the property argument by the values
      corresponding to each constructor.]

    - Enough for [ev_minus2'], because [n] is mentioned in the final goal.

    - Doesn't help for [evSS_ev], since the replaced term ([S (S n)]) is not
      mentioned anywhere. *)

(** In standard [Ltac], the tactic used in such cases is [inversion], which can
    detect (1) that the first case does not apply, and (2) that the [n'] that
    appears on the [ev_SS] case must be the same as [n]. *)

Theorem evSS_ev : forall n,
  ev (S (S n)) -> ev n.
Proof.
  move => n E.
  inversion E as [| n' E'].
  (* We are in the [E = ev_SS n' E'] case now. *)
  apply E'.
Qed.

(** But, as stated on several previous occasions, [ssreflect] avoids the use of
    [inversion] by design. In fact, we have arrived at a point where a more
    systematic discussion of principles underlying [ssreflect] is necessary... *)


(* ================================================================= *)
(** ** Full Syntax of Case Distinction *)

(** Let us try to prove the previous theorem about evenness using
    [ssreflect] methodology: *)

Theorem evSS_ev' : forall n,
  ev (S (S n)) -> ev n.
Proof.
  move => n E.

 
  (**
   -  Recall that [inversion] on [E] pulled new equations out of the hat
      by inspecting the assumption and applying some heuristics. 
   -  In [ssreflect],
      we would like to do that in a more coordinated way. 
   -  We want to do case
      distinction on the constructors of [E], instantiating them with [S (S n)] as
      their parameter. 
   -  This can be done using the [case: ... / ...] variant of
      the [case] tactic: *)

  
  case E' : (S (S n)) / E.

  (** Note that for example [ case E' :  (S n) / E] results in error: "the given pattern matches the term (S n) while the inferred pattern 
(S (S n)) doesn't" *)

  (** *** *)

  (** The first case, where [S (S n) = 0], is solved by [discriminate], or
      the [by] tactical: *)
  
  - by [].

  (** We are now in the case where our pattern [(S (S n))] is replaced by
      the _matching_ conclusion of the [ev_SS] constructor (with an anonymous
      variable) and we finish in a standard way. **)
  
  - by case: E' => -> .

    (** we could write [case E' : (S (S n)) / E => [|n' en']] to give names to things. *)
Qed.

(** By using this methodology, we can also apply the principle of explosion to
    "obviously contradictory" hypotheses involving inductive properties. For
    example: *)

Theorem one_not_even : ~ ev 1.
Proof.
  by case E : _ /. Qed.

(** Prove the following results using [case: ... / ...]. *)

Theorem SSSSev__even : forall n,
  ev (S (S (S (S n)))) -> ev n.
Proof.
  (* WORK IN CLASS *) Admitted.


(** An even simpler theorem: *)

Theorem even5_nonsense :
  ev 5 -> 2 + 2 = 9.
Proof.
  case E1: _ / => [| n Hn] //. 
  case E2: _ / Hn => [|n' Hn'] //; rewrite E2 in E1.
  (* WORK IN CLASS *) Admitted.     






(** This look repetive. Let us smoothen this up a bit: *)

Theorem even5_nonsense' :
  ev 5 -> 2 + 2 = 9.
Proof.
  (*case E1: 5 / => [| n Hn] //. 
  case E2: _ / Hn => [|n' Hn'] *)
  case E1: _ / => [| n Hn] //.
  case: E1 Hn => <-. (* Automating further: *)
  
   (* WORK IN CLASS *) Admitted.

(** But this looks repetitive too. If we don't insist on naming the number and use the [do] tactical, we can finally reach perfection ... *)



(** Here we go! *)



Theorem even5_nonsense'' :
  ev 5 -> 2 + 2 = 9.
Proof.
  do 3!(case E1: _ / => [| ? Hn] //;
  case: E1 Hn => <-).
Qed.

(* ================================================================= *)
(** ** Boolean Reflection *)

Import Nat.

(** Let us quickly prove some facts about evenness. Recall again [~~] in [ssreflect] is rather misleading. *)

Theorem even_S : forall n : nat, even (S n) = ~~ (even n).
Proof.
  elim=> /= [|n IH].
  - by [].
  - by rewrite IH; case: (even n).
Qed.

(** This one is actually quite instructive: *)

Theorem even_double_conv : forall n, exists k, n = if even n then double k else (S (double k)).
Proof.
  (** Hint: using the lemma above and destructing compound expressions might help. 
      Also, standard library lemmas about adding successor... *)
  (* WORK IN CLASS *) Admitted.

(** The following is just a copy and paste of an earlier exercise: **)
Theorem ev_double : forall n,
  ev (double n).
Proof.
  (* WORK IN CLASS *) Admitted.

(** There is an obvious distinction between boolean values and elements
    of [Prop].

    - Logical connectives in [Prop] are _type operators_

    - Logical connectives in [bool] are _functions_ 

    Proofs about propositions in [bool], as we have seen, are often simple
    applications of [case]: *)

Lemma b_or_not_b : forall b : bool, b || ~~b = true.
Proof. by case. Qed.

(** Thus, [Prop] and [bool] are complementary: [Prop] allows robust natural
    deduction, [bool] allows brute-force evaluation.

    Coq automatically coerces booleans into propositions using the _coercion_
    mechanism: *)

(* Coercion is_true (b : bool) := b = true. *)
Check is_true. (* => Bool -> Prop *)

(** Coercions require some care... they can lead to some serious confusions with output and pretty printing!  *)

(** Going back to the discussion of evenness, we can now ask the question
    how to pass from one representation of evenness to another. This is one of
    the main features of [ssreflect] and it is embodied in the [reflect]
    predicate: *)

Print reflect.
(* ==> 
   Inductive reflect (P : Prop) : bool -> Set :=
       ReflectT : P -> reflect P true
     | ReflectF : ~ P -> reflect P false *)

(** The statement (reflect P b) asserts that (is_true b) and P are logically
    equivalent propositions. *)

(** Let us look at the most simple evidence for [reflect]: *)

Check idP.
Lemma idP' : forall b : bool, reflect b b.
  Proof. by case; [left | right]. Qed.

(** Notice that the first [b] in [reflect b b] is actually the proposition
    [is_true b]! *)

(** This is actually part of the [ssreflect] libraries, together with another
    important candidate: *)

Check iffP.
Lemma iffP' : forall (P Q : Prop) (b : bool),
    reflect P b -> (P -> Q) -> (Q -> P) -> reflect Q b.
Proof.
  move=> P Q b. case; move=> p PQ QP.
  by left; apply (PQ p).
  by right; move=> q; have F := QP q.
Qed.

(** In other words, we can relate two propositions with the same boolean if we
    give a biimplication between the propositions. *)

(** Now we can state our first so-called _reflection view_, a lemma witnessing
    the equivalence between a proposition and a boolean: *)

Lemma evenP : forall n : nat, reflect (exists k, n = double k) (even n).
Proof.
  move=> n.
  apply: (iffP idP).
  - move=> H.
    case: (even_double_conv n) => [k Hk].
    by rewrite Hk H; exists k.
  - move=> [k Hk]. rewrite {}Hk.
    elim: k => [|k IHk] => //.
    by rewrite /double [(S k) + _]add_succ_r add_succ_l; apply: IHk.
Qed. 

(** We will learn how to use it efficiently now. *)

(* ================================================================= *)
(** ** Induction on Evidence *)

(** The earlier [ev_double] exercise shows that our new notion
    of evenness is implied by the two earlier ones. To show that all three
    coincide, we just need the following lemma: *)
    
Lemma ev_even : forall n,
  ev n -> exists k, n = double k.
Proof.
  move=> n E.

(** Let us use [apply/...] to interpret this goal as a boolean
    proposition: *)

  apply/evenP.

(** We could try case/induction on [n], but [ev] is mentioned in [E], so
    probably dead end. Maybe [case] on evidence for [ev]? *)
  
  case H: _ / E => [|n' E'] => //.

(** The second case seems that the original goal! However, after simplification, we
    see that we obtain the same goal as we started with, but with [n'] instead
    of [n]. *)

    rewrite /=.

(** If this looks familiar, it is no coincidence: We've encountered
    similar problems in the [Induction] chapter, when trying to use
    case analysis to prove results that required induction.  And once
    again the solution is... induction! *)

(** This time, however, it's induction on evidence. *)

      by elim E'.

(** Any idea why it was possible to finish the job so quickly? *)
      
Qed.

(**    
    The behavior of [induction] on evidence is the same as its
    behavior on data: It causes Coq to generate one subgoal for each
    constructor that could have been used to build that evidence, while
    providing an induction hypotheses for each recursive occurrence of
    the property in question.

    Let us try the last proof again in more detail: *)

Lemma ev_even' : forall n,
  ev n -> exists k, n = double k.
Proof.
  move=> n E.
  apply/evenP.
  elim: E => [|n' E' IH]; first by [].

(** Notice the induction hypothesis talks about [n'], the number
    mentioned by [E']. *)

  by [].
Qed.

(** As we will see later, induction on evidence is a
    recurring technique when studying the semantics of programming
    languages, where many properties of interest are defined
    inductively. *)

(** The equivalence between the second and third definitions of evenness now
    follows. In other words, we can easily add a reflection view for [ev n] as
    well. *)

Lemma evP : forall n : nat, reflect (ev n) (even n).
Proof.
  move=>n.
  apply/(iffP idP).
  - move/evenP.
    move=> [k Hk].
    rewrite Hk.
    by apply ev_double.
  - move=> ?.
    apply/evenP.
    by apply: ev_even.
Qed.

(* ----------------------------------------------------------------- *)
(** *** Exercises *)

(** **** Exercise: 2 stars, standard (ev_even_iff)  *)
Theorem ev_even_iff : forall n,
  ev n <-> exists k, n = double k.
Proof.
  (* FILL IN HERE *) Admitted.
(** [] *)

(** **** Exercise: 2 stars, standard (ev_sum)  *)
Theorem ev_sum : forall n m, ev n -> ev m -> ev (n + m).
Proof.
  (* FILL IN HERE *) Admitted.
(** [] *)

