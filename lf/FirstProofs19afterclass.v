(** * FirstProofs19: First real proofs in Coq *)

(** Adapted from the Software Foundations for SemProg@FAU 2013--2019 *)

(** Here is how you load material from the previous lecture. 
 For the more sophisticated route we follow here, you first need following steps:
  - download the [_CoqProject] file that I posted on StudOn and save it in the same folder
  - write in console 

    coq_makefile -Q . LF19 Basics19.v FirstProofs19.v -o Makefile; make

  
Did not work? A fallback dirty option is to "make clean", rename the "[_CoqProject]" file to something dummy, kill "From LF19" in front of Require, and compile Basics19 manually with coqc. But it's absolute last resort.   
  *)

From LF19 Require Export Basics19.

(** Let us also load [ssreflect] again. *)

Set Warnings "-notation-overridden,-parsing".
From Coq Require Import ssreflect ssrfun ssrbool.



(* ################################################################# *)
(** * Proof by Simplification *)

(** A general property of natural numbers: *)

Theorem plus_O_n : forall n : nat, 0 + n = n.
Proof. intros n. simpl. 
  reflexivity.  Qed. 

Theorem plus_O_n' : forall n : nat, 0 + n = n.
Proof.
  by []. Qed.

Theorem plus_1_l : forall n:nat, 1 + n = S n. by []. Qed.

Theorem mult_0_l : forall n:nat, 0 * n = 0. reflexivity . Qed.

(** The [_l] suffix in the names of these theorems is
    pronounced "on the left." *)

(** Unfortunately, proof by simplification doesn't always work. *)

Theorem plus_n_O : forall n, n = n + 0.
Proof.
  intros n. 

  Fail reflexivity.

  (** We can finish it in the [ssreflect] way though... *)

  by [].
  
Qed.

(** We will see another way to deal with this later. *)

(* ################################################################# *)
(** * Proof by Rewriting *)

(** A slightly more interesting theorem: *)

Theorem plus_id_example : forall n m:nat,
  n = m ->
  n + n = m + m.

(** This theorem shows that [n = m] _implies_ that [n + n = m +
    m], and we read the arrow symbol as "implies." *)

(**  For this proof, we use [intros] to move all of the hypotheses from
    the goal to the context.

    The [rewrite] tactic tells Coq to replace an expression in the
    goal with one that we know is equal to it. *)

Proof.
  (* move both quantifiers and the hypothesis into the context: *)
  intros n m.
  intros H.  Fail reflexivity.
  (* rewrite the goal using the hypothesis: *)
  rewrite -> H.
  reflexivity. Qed.

(** The uses of [intros] name the hypotheses as they are moved
    to the context.  The [rewrite] needs to know which equality is being
    used and in which direction to do the replacement. *)

(** Here's how to prove this theorem in the [ssreflect] style. *)

Theorem plus_id_example' : forall n m:nat,
  n = m ->
  n + n = m + m.

Proof.
  by
    move=> n m H;
            rewrite H. Qed.

(** The use of [move =>] has its advantages, especially regarding naming conventions. Will discuss it in more detail when we have a better background later. *) 

(** Remove "[Admitted.]" and fill in the proof. *)

Theorem plus_id_exercise : forall n m o : nat,
  n = m -> m = o -> n + m = m + o.
Proof.  
  move => n m o Hnm Hmo.
    by rewrite Hnm Hmo.
Qed.    


(** We can also use the [rewrite] tactic with a previously proved
    theorem instead of a hypothesis from the context. If the statement
    of the previously proved theorem involves quantified variables,
    as in the example below, Coq tries to instantiate them
    by matching with the current goal. *)

Theorem mult_double : forall p q : nat,
  p = q -> (p + p) * q = (q + q) * q.
Proof.
  by move=> p q EQpq; rewrite EQpq.
Qed.

(* ################################################################# *)
(** * Proof by Case Analysis *)

(** Sometimes simplification and rewriting are not enough... *)

Theorem plus_1_neq_0_firsttry : forall n : nat,
  (n + 1 =? 0) = false.
Proof.
  intros n.

  Fail reflexivity.
Abort.

(** This time, [ssreflect] finds no magic bullet either... *)

Theorem plus_1_neq_0_firsttry' : forall n : nat,
  (n + 1 =? 0) = false.
Proof.
  
  move=> n.

  Fail by [].

Abort.

    
(**
    To make progress, we need to consider the possible forms of [n].

    The tactic that tells Coq to consider, separately, the cases where
    [n = O] and where [n = S n'] is called [destruct]. *)

Theorem plus_1_neq_0 : forall n : nat,
  (n + 1 =? 0) = false.
Proof.
  intros n. destruct n as [| n']. 
  - reflexivity. 
  - reflexivity. 
Qed.

(** Note the "bullets" marking the proofs of the two subgoals. *)

(** There is syntactic sugar for doing [destruct] following [intros]: *)

Theorem plus_1_neq_0' : forall n : nat,
  beq_nat (n + 1) 0 = false.
Proof.
  intros [| n']. 
   - reflexivity. 
   - reflexivity.   
Qed.

(** There is a short way to throw the same tactic on every subgoal. It is provided by the [;] _tactical_. "Tacticals" are tactics that take other tactics as
    arguments -- "higher-order tactics," if you will. We have already seen an example of one: it was [by] of [ssreflect]. *)

Theorem plus_1_neq_0'' : forall n : nat,
  beq_nat (n + 1) 0 = false.
Proof.
  intros [| n'];  reflexivity.    
Qed.

(** Again, [ssreflect] provides an even shorter way to split things by using [case]. *)

Theorem plus_1_neq_0''' : forall n : nat,
  (n + 1 =? 0) = false.
Proof. by case.
Qed.

(**
    The [destruct] tactic and its relatives can be used with any inductively defined
    datatype.  For example, we use them next to prove that boolean
    negation is involutive -- i.e., that negation is its own
    inverse. *)

Theorem negb_involutive : forall b : bool,
  negb (negb b) = b.
Proof.
  intros []; reflexivity .
Qed.

(** ... or, alternatively... *)

Theorem negb_involutive' : forall b : bool,
  negb (negb b) = b.  by case.
Qed.

(** We can have nested subgoals (and we use different "bullets"
    to mark the inner ones): *)

Theorem andb_commutative_boring : forall b c, b && c = c && b.
  destruct b.
  - destruct c. 
     + reflexivity. 
     + reflexivity. 
  - destruct c.
     + reflexivity. 
     + reflexivity.
Qed. 

(** Better... *)

Theorem andb_commutative : forall b c, b && c = c && b.
Proof.
  intros [] []; reflexivity.
Qed.

(** ... and in the [ssreflect] style ... *)

Theorem andb_commutative' : forall b c, b && c = c && b.
  by do 2! case.
Qed.

(** Similarly... *)


Theorem andb3_exchange:
  forall b c d, (b && c) && d = (b && d) && c.
Proof.
  intros [] [] []; reflexivity.
Qed.

Theorem andb3_exchange' :
  forall b c d, (b && c) && d = (b && d) && c.
Proof.
    by do 3! case.
Qed.

(* ################################################################# *)
(** * More Exercises *)

(** **** Exercise: 2 stars, standard (identity_fn_applied_twice)  

    Use the tactics you have learned so far to prove the following
    theorem about boolean functions. *)

Theorem identity_fn_applied_twice :
  forall (f : bool -> bool),
  (forall (x : bool), f x = x) ->
  forall (b : bool), f (f b) = b.
Proof.
  move => f hip b.
    by do 2! rewrite hip.
Qed.    



(** Now state and prove a theorem [negation_fn_applied_twice] similar
    to the previous one but where the second hypothesis says that the
    function [f] has the property that [f x = negb x].*)

(* FILL IN HERE *)

(** [] *)


(* ================================================================= *)
(** ** Proof by Induction *)

(** Let us now return to the type of problems we began with: seemingly trivial reordering making theorem impossible to prove. *)

Check plus_1_l.

Theorem plus_1_r_attempt : forall n, S n = n + 1.
Proof.
  Fail reflexivity.
  intros [].
  - reflexivity.
  - Fail reflexivity.
Abort.


(** [ssreflect] also cannot handle examples such as this one using tactics we've learned so far: *)
  
Theorem plus_1_r_ssr_attempt : forall n, S n = n + 1.
Proof.
  Fail by [].
  Fail by case.

Abort.

(** ... or such as this one ... *)

Theorem minus_diag : forall n,
  minus n n = 0.
Proof.
  Fail by [].
  Fail by case.
Abort.  

(** So we're gonna need a bigger boat. *)


(** Our "bigger boat" is the _principle of induction_ over
    natural numbers...

      - If [P(n)] is some proposition involving a natural number [n],
        and we want to show that [P] holds for _all_ numbers, we can
        reason like this:

         - show that [P(O)] holds
         - show that, if [P(n')] holds, then so does [P(S n')]
         - conclude that [P(n)] holds for all [n].

    In Coq's "goal-directed" style of reasoning, we begin with the
    goal of proving [P(n)] for all n and break it down (by applying
    the [induction] tactic) into two separate subgoals: first showing
    [P(O)] and then showing [P(n') -> P(S n')].  For example... *)

Theorem plus_n_O_with_ltac : forall n:nat, n = n + 0.
Proof.
  intros n. induction n as [| n' IHn'].
  - (* n = 0 *)    reflexivity.
  - (* n = S n' *) simpl. rewrite <- IHn'. reflexivity.
Qed.

(* ----------------------------------------------------------------- *)
(** *** Induction in [ssreflect] *)


(** [ssreflect] avoids the tactic [induction] for same 
    reasons as several other ones from [Ltac]: it gives you limited control over the context and bookkeeping. Instead, the more basic induction tactic [elim] is
    preferred. Let us return to one of examples we couldn't do.  *)

Theorem plus_1_r : forall n, S n = n + 1.
Proof.
  elim=> [| n IHn] /= //. (** <- this last thing kills trivial subgoals *)
  (* n = S n' *) by rewrite IHn.
Qed.


(** We see here how on-the-fly simplification is done in
    [ssreflect] (using the switch [/=]). *) 

(** Let's try the other one we couldn't do: *)

Theorem minus_diag : forall n,
  minus n n = 0.
Proof.
 by elim => [] //.
Qed.
(* ----------------------------------------------------------------- *)
(** *** More uses of induction *)

(** Prove the following. *)

Theorem mult_0_r : forall n:nat,
  n * 0 = 0.
Proof.
  by elim => [] //.
Qed.

(** Now it's time for associativity. It's slightly more complicated, but really only slightly so. *)

Theorem plus_assoc : forall n m p : nat,
  n + (m + p) = (n + m) + p.
Proof.
  move => n ? ?.
  revert n.
  elim => [|n' Hn'] /= //.
    by rewrite Hn'.
Qed.    
  (* This is how you do "anonymous intros" in [ssreflect].  *)
 

(** How abour commutativity? Here, as it turns out, we need one more little insight... *)

(** Recall our [plus_n_O]? It is not so interesting in its own right. But it's very
    often the case we need such auxiliary lemmas for more interesting
    inductive proofs, such as the one below.  *)

(** First, let us see another lemma already in the standard library... *)

Check plus_n_Sm.

(* =>
plus_n_Sm
     : forall n m : nat, S (n + m) = n + S m *)

Theorem plus_comm : forall n m : nat,
  n + m = m + n.
Proof.
  elim => [|n' Hn] /= //.
  move => m.
    by rewrite Hn plus_n_Sm.
Qed.    

Theorem plus_comm' : forall n m : nat,
  n + m = m + n.
Proof.
  move => n. elim => [|m' Hm] /= //.
    by rewrite -Hm => /=.
Qed.    



(* ################################################################# *)
(** * Proofs Within Proofs *)

(** Here's a proof using an in-line
    assertion instead of a separate lemma.  New tactic: [assert]. *)

Theorem mult_0_plus' : forall n m : nat,
  (0 + n) * m = n * m.
Proof.
  intros n m.
  assert (H: 0 + n = n). { reflexivity. }
  rewrite -> H.
  reflexivity.  Qed.

(** The [assert] tactic is handy in many sorts of situations ... *)

(* ----------------------------------------------------------------- *)
(** *** Rewriting guided by assertions *)

Theorem plus_rearrange_firsttry : forall n m p q : nat,
    (n + m) + (p + q) = (m + n) + (p + q).

(**
    The only difference between the two sides of the
    [=] is that the arguments [m] and [n] to the first inner [+] are
    swapped, so it seems we should be able to use the commutativity of
    addition ([plus_comm]) to rewrite one into the other.  However,
    the [rewrite] tactic is a little stupid about _where_ it applies
    the rewrite.  There are three uses of [+] here, and it turns out
    that doing [rewrite -> plus_comm] will affect only the _outer_
    one... *)

Proof.
  intros n m p q.
  (* We just need to swap (n + m) for (m + n)...
     it seems like plus_comm should do the trick! *)
  rewrite -> plus_comm.
  (* Doesn't work...Coq rewrote the wrong plus! *)
Abort.

(** To get [plus_comm] to apply at the point where we want it to, we
    can introduce a local lemma stating that [n + m = m + n] (for the
    particular [m] and [n] that we are talking about here), prove this
    lemma using [plus_comm], and then use it to do the desired
    rewrite. *)

Theorem plus_rearrange : forall n m p q : nat,
  (n + m) + (p + q) = (m + n) + (p + q).
Proof.
  intros n m p q.
  assert (H: n + m = m + n).
  { rewrite -> plus_comm. reflexivity. }
  rewrite -> H. reflexivity.  Qed.

(* ----------------------------------------------------------------- *)
(** *** Rewriting eased by [ssreflect] *)

(** Introducing a separate assertion just for rewriting purposes can
    quickly become cumbersome. This is another place where [ssreflect]
    shows its strength, namely with its extended [rewrite] tactic:
      - performs rewriting, simplifications, folding/unfolding of definitions
      - can chain rewriting operations
      - enhanced occurrence selection *)

(**
   For example, the proof of [plus_rearrange] above can be shortened
   as follows: *)

Theorem plus_rearrange' : forall n m p q : nat,
  (n + m) + (p + q) = (m + n) + (p + q).
Proof.
  move=> n m.
  by rewrite [n + _]plus_comm.
Qed.

(** The _rewrite pattern_ in square brackets can be (among other
    things not important at this stage) any term. *)

(* ================================================================= *)
(** ** Induction beyond natural numbers *)

(** As you can imagine, we can do induction on any inductive type. Here is one example: *)

(** We can generalize our unary representation of natural numbers to
    the more efficient binary representation by treating a binary
    number as a sequence of constructors [A] and [B] (representing 0s
    and 1s), terminated by a [Z]. For comparison, in the unary
    representation, a number is a sequence of [S]s terminated by an
    [O]. *)

(** ** *)

(**
    For example:

        decimal            binary                           unary
           0                   Z                              O
           1                 B Z                            S O
           2              A (B Z)                        S (S O)
           3              B (B Z)                     S (S (S O))
           4           A (A (B Z))                 S (S (S (S O)))
           5           B (A (B Z))              S (S (S (S (S O))))
           6           A (B (B Z))           S (S (S (S (S (S O)))))
           7           B (B (B Z))        S (S (S (S (S (S (S O))))))
           8        A (A (A (B Z)))    S (S (S (S (S (S (S (S O)))))))

    Note that the low-order bit is on the left and the high-order bit
    is on the right -- the opposite of the way binary numbers are
    usually written.  This choice makes them easier to manipulate. *)

Inductive bin : Type :=
  | Z
  | A (n : bin)
  | B (n : bin).

Fixpoint bin_to_nat (m:bin) : nat :=
  match m with
  | Z    => O
  | A m' => 2 * bin_to_nat m'
  | B m' => 1 + 2 * bin_to_nat m'
  end.

(** (a) Complete the definition below of an increment function [incr]
        for binary numbers. *)

Fixpoint incr (m:bin) : bin
  (* REPLACE THIS LINE WITH ":= _your_definition_ ." *). Admitted.


Example test_bin_incr1 : (incr (B Z)) = A (B Z).
  (* FILL IN HERE *) Admitted.

Example test_bin_incr2 : (incr (A (B Z))) = B (B Z).
  (* FILL IN HERE *) Admitted.

Example test_bin_incr3 : bin_to_nat (A (B Z)) = 2.
  (* FILL IN HERE *) Admitted.

Example test_bin_incr4 :
        bin_to_nat (incr (B Z)) = 1 + bin_to_nat (B Z).
  (* FILL IN HERE *) Admitted.

Example test_bin_incr5 :
        bin_to_nat (incr (incr (B Z))) = 2 + bin_to_nat (B Z).
  (* FILL IN HERE *) Admitted.

Theorem incr_is_S: forall m:bin, bin_to_nat (incr m) = S (bin_to_nat m). 
  (* FILL IN HERE *) Admitted.

