(** * Tactics19: More Basic Tactics *)

(** Adapted from _Software Foundations_  for SemProg@FAU 2013--2019 *)

(** This chapter introduces several additional proof strategies
    and tactics that allow us to begin proving more interesting
    properties of functional programs.  We will see:

    - how to use auxiliary lemmas in both "forward-style" and
      "backward-style" proofs;
    - how to reason about data constructors (in particular, how to use
      the fact that they are injective and disjoint);
    - how to strengthen an induction hypothesis (and when such
      strengthening is required); and
    - more details on how to reason by case analysis. *)

Set Warnings "-notation-overridden,-parsing".
From Coq Require Import ssreflect ssrbool ssrfun.

(** And instead of our list development in the [Poly19] chapter, let's go for the standard library ... *)

From Coq Require Import List.
Import List.ListNotations.

(** ... and also, for [beq_nat], instead of loading it from [Basics19.v] ... *)

Import Nat.

(** ... and also, for [beq_nat] ... *)

From Coq Require Import Arith.
Notation "x =? y" := (beq_nat x y) (at level 70) : nat_scope.
Notation "x <=? y" := (leb x y) (at level 70) : nat_scope. (* [leb] comes from [Nat] *)

(* ================================================================= *)
(** ** The [apply] Tactic *)

(** We can use [apply] when some hypothesis or an earlier
    lemma exactly matches the goal: *)

Theorem silly1 : forall (n m o p : nat),
     n = m  ->
     [n;o] = [n;p] ->
     [n;o] = [m;p].
Proof.
(** Interlude: Writing an arrow instead of an identifier after a [=>]
    tactical tries to rewrite immediately using the top of the proof
    stack. So the following _anonymously_ introduces the four natural
    numbers, introduces [n = m] temporarily and rewrites the rest of
    the goal with it. *)

  move=> ? ? ? ? <-.

(** Recall the variables introduced anonymously have special names.
    These kinds of names are reserved notation and _cannot_ be used
    any further in the proof. *)

(** Here, we could finish with "[move=> eq. rewrite eq.  by [].]" as we
    have done several times before.  We can achieve the same effect in
    a single step by using the [apply] tactic instead: *)

  by apply. Qed.

(** [apply] also works with _conditional_ hypotheses. Here's [ssreflect] style: *)

Theorem silly2 : forall (n m o p : nat),
     n = m  ->
     (forall (q r : nat), q = r -> [q;o] = [r;p]) ->
     [n;o] = [m;p].
Proof.
  move=> ? ? ? ? eq. apply. apply: eq. Qed.

(** ... and here's [Ltac] style: *)

Theorem silly2' : forall (n m o p : nat),
     n = m  ->
     (forall (q r : nat), q = r -> [q;o] = [r;p]) ->
     [n;o] = [m;p].
Proof.
  

  (** ... see these confusing names below btw? Before we get here with CoqIDE/Proof General, do you see what they refer to? *)
  
  intros. apply H0. apply H. Qed.



(** Observe how Coq picks appropriate values for the
    [forall]-quantified variables of the hypothesis: *)

Theorem silly2a : forall (n m : nat),
     (n,n) = (m,m)  ->
     (forall (q r : nat), (q,q) = (r,r) -> [q] = [r]) ->
     [n] = [m].
Proof.
  move=> n m eq1 eq2. apply: eq2. apply: eq1. Qed.

(** The goal must match the hypothesis _exactly_ for [apply] to
    work: *)

Theorem silly3_firsttry : forall (n : nat),
     true = (n =? 5)  ->
     (S (S n) =? 7) = true.
Proof.
  move=> n H.

  (** Note btw how [ssreflect] transferred [beq_nat]! *)

  Fail apply H.

(** Here we cannot use [apply] directly, but we can use the [symmetry]
    tactic, which switches the left and right sides of an equality in
    the goal. *)

  symmetry. apply: H. Qed.

(* ================================================================= *)
(** ** [apply ... with ...] vs. [apply] in [ssreflect] *)

(** The standard library is aware that equality is transitive: *)

Check trans_eq.

(** In [Ltac], applying this lemma to the following example  requires a slight
    variation on [apply]: *)

Example trans_eq_example : forall (a b c d e f : nat),
     [a;b] = [c;d] ->
     [c;d] = [e;f] ->
     [a;b] = [e;f].
Proof.
  intros a b c d e f eq1 eq2.

  Fail apply trans_eq.
  

  
  
  (* Doing [apply trans_eq] doesn't work!  But... *)
    apply trans_eq with (y:=[c;d]).
  (* does. *)

  apply eq1. apply eq2.   Qed.

(** Here's how to play this in [ssreflect]: *)

Example trans_eq_example' : forall (a b c d e f : nat),
     [a;b] = [c;d] ->
     [c;d] = [e;f] ->
     [a;b] = [e;f].
Proof.
  move => ? ? ? ? ? ? eq1 eq2.
  apply: trans_eq.

  (* See what happened? *)

  apply: eq1.

  (* ... aaaand we're done *)
  
    by apply: eq2.   Qed.

(** In fact, we can make it even more compact: *)

Example trans_eq_example'' : forall (a b c d e f : nat),
     [a;b] = [c;d] ->
     [c;d] = [e;f] ->
     [a;b] = [e;f].
Proof.
  move => ? ? ? ? ? ? eq1 eq2.
  by apply: trans_eq eq1 eq2.
Qed.

(** Let us practice this use of [apply] on the following simple example: *)

Definition minustwo (n : nat) : nat :=
  match n with
  | O => O
  | S O => O
  | S (S n) => n
  end.


Example trans_eq_exercise : forall (n m o p : nat),
     m = (minustwo o) ->
     (n + p) = m ->
     (n + p) = (minustwo o).
Proof.
  (* WORK IN CLASS *) Admitted.

(* ================================================================= *)
(** ** Injective and Disjoint Constructors *)

(** In Coq, the constructors of inductive types are _injective_
    and _disjoint_.

    E.g., for [nat]...

    - the only way we can have [S n = S m] is if [n = m] (that is, [S]
      is _injective_)

    - [O] is not equal to [S n] for any [n] (that is, [O] and [S] are
      _disjoint_)
*)

(** The [injection] and [discriminate] tactics allow us to
    invoke these principles in proofs: *)

Theorem S_injective : forall (n m : nat),
  S n = S m ->
  n = m.
Proof.
  move=> n m H.

  injection H.

  (* At this state, we're essentially done. 
   But for the sake of hygiene, let us remove used hypothesis ... *)
  
  clear H.

  (* ... and say goodbye. *)

    by [].
    
Qed.

(** Here's a more interesting example that shows how multiple
    equations can be derived at once. *)

Theorem injection_ex1 : forall (n m o : nat),
  [n; m] = [o; o] ->
  [n] = [m].
Proof.
  move=> n m o H.
  injection H => eq1 eq2.
  by rewrite eq1 eq2.
Qed.

(** [ssreflect]'s [case] tactic calls the [injection] tactic if
    the object to be destructed is an equation, essentially rendering
    the use of [injection] unnecessary, reducing the set of tactics to
    remember: *)

Theorem injection_ex1' : forall (n m o : nat),
  [n; m] = [o; o] ->
  [n] = [m].
Proof.
  move=> n m o.
  case=> eq1 eq2.
  by rewrite eq1 eq2.
Qed.

Example case_ex3 : forall (X : Type) (x y z : X) (l j : list X),
  x :: y :: l = z :: j ->
  y :: l = x :: j ->
  x = y.
Proof.
  (* WORK IN CLASS *) Admitted.

(** The [discriminate] tactic allows us to use the disjointness
    property of constructors: *)

Theorem discriminate_ex1 : forall (n m o : nat), S n = O -> m = o.
Proof.
  move=> n m o H.
  discriminate H.
Qed.

(** [ssreflect]'s [by] terminator calls [discriminate] by
    itself, so we also don't need to remember the [discriminate]
    tactic: *)

Theorem discriminate_ex1' : forall (n m o : nat), S n = O -> m = o.
Proof.
  by [].
Qed.

(** This is an instance of a logical principle known as the _principle
    of explosion_ or, in Latin, _ex falso quodlibet_, which asserts that a contradictory hypothesis
    entails anything, even false things! *)

Theorem explosion_ex4 : forall (n : nat),
  S n = O ->
  2 + 2 = 5.
Proof. by []. Qed.

Theorem explosion_ex5 : forall (n m : nat),
  false = true ->
  [n] = [m].
Proof. by []. Qed.

(** To summarize this discussion, suppose [H] is a hypothesis in the
    context or a previously proven lemma of the form [[ c a1 a2 ... an
    = d b1 b2 ... bm ]] for some constructors [c] and [d] and
    arguments [a1 ... an] and [b1 ... bm]. Then:

    - If [c] and [d] are the same constructor, then, by the
      injectivity of this constructor, we know that [a1 = b1], [a2 =
      b2], etc. [case: H] adds these facts to the top of the goal.

    - If [c] and [d] are different constructors, then the hypothesis
      [H] is contradictory, and the current goal doesn't have to be
      considered at all. In this case, [by []] marks the current goal
      as completed. *)

(** [Ltac] has another powerful tactic incorporating these principles. It is called [inversion]. It goes completely against the philosophy you've been seeing in [ssreflect], it's quite messy and can moreover slow down significantly larger proofs. We will avoid it... when we can. But sometimes this big hammer comes really handy. *) 

(* QUIZ 

    Suppose Coq's proof state looks like

         1 subgoals, subgoal 1

         x : bool
         y : bool
         H : negb x = negb y
         ============================
          y = x

    and we apply the tactic [case: H => H].  What will happen?

    (1) No new information available in the context

    (2) No new information, but a warning occurs.

    (3) Substitute [x] with [y] in the conclusion, leaving
        [y = y]

    (4) "No more subgoals"
*)
(* /QUIZ *)

(* QUIZ 

    Suppose Coq's proof state looks like

             1 subgoals, subgoal 1

             H : negb false = negb true
             ============================
              true = false

    and we apply the tactic [by []].  What will happen?

    (1) No new information available in the context

    (2) "No more subgoals"
*)
(* /QUIZ *)

(** Digression: A useful convenience theorem that goes in the
    "reverse direction" from injection or inversion... *)

Check f_equal.
Theorem f_equal_reproved : forall (A B : Type) (f: A -> B) (x y: A),
  x = y -> f x = f y.
Proof. by move=> ? ? ? ? ? ->. Qed.

(* ================================================================= *)
(** ** Using Tactics on Hypotheses *)

(** [ssreflect] comes with a tactical "[in]" that can be used
    to apply tactics in hypotheses instead of goals. *)

Theorem S_inj : forall (n m : nat) (b : bool),
     (S n) =? (S m) = b  ->
     n =? m = b.
Proof.
  move=> n m b H. move=> /= in H. by []. Qed.

(** [in] is a powerful and versatile tactical in [ssreflect], but numerous tactics can be combined with [in] also in [Ltac] style: *)

Theorem S_inj' : forall (n m : nat) (b : bool),
     (S n) =? (S m) = b  ->
     n =? m = b.
Proof.
  intros. simpl in H. apply H. Qed.

(**  
  - The ordinary [apply] tactic is a form of "backward
    reasoning" 
  - it says "We're trying to prove [X] and we know [Y->X],
    so if we can prove [Y] we'll be done." 
  - We can apply hypotheses to
    other hypotheses to obtain a form of "forward reasoning" 
  - "We know
    [Y] and we know [Y->X], so we can deduce [X]." *)

Theorem silly3' : forall (n : nat),
  (n =? 5 = true -> (S (S n)) =? 7 = true) ->
  true = (n =? 5)  ->
  true = ((S (S n)) =? 7).
Proof.
  move=> n eq H.

  (* Note that the equalities in the hypotheses are oriented
     differently, so let's match them up first. *)

  symmetry in H. symmetry.

  (* Now the conclusion of the hypothesis [eq] lines up with our goal
     and the premise matches [H], so we can _apply_ (in the
     mathematical sense, not the tactic) [H] to [eq] and use [apply]
     to finish. *)

  apply: eq H. Qed.

(** There are numerous functions in the standard library that can help you here, such as [Nat.add_succ_r] or [Nat.add_succ_l] ... *)

Theorem plus_n_n_injective : forall n m,
     n + n = m + m ->
     n = m.
Proof.
  (* WORK IN CLASS *) Admitted.

(* ================================================================= *)
(** ** Varying the Induction Hypothesis *)

(** Here's a function for doubling a natural number: *)

Fixpoint double (n:nat) :=
  match n with
  | O => O
  | S n' => S (S (double n'))
  end.

(** Suppose we want to show that [double] is injective -- i.e.,
    that it maps different arguments to different results.  The
    way we _start_ this proof is a little bit delicate: *)

Theorem double_injective_FAILED : forall n m,
     double n = double m ->
     n = m.
Proof.
  move=> n m. elim: n => [| n IH].
  - (* n = O *) by case: m.
  - (* n = S n' *) case: m IH => [ | m' IH].
    + (* m = O *) by [].
    + (* m = S m' *) move=> eq. apply: f_equal.

(** At this point, the induction hypothesis, [IH], does _not_ give us
    [n = m'] -- there is an extra [S] in the way -- so the goal is
    not provable. *)

      Abort.

(** What went wrong? *)

(** Trying to carry out this proof by induction on [n] when [m] is
    already in the context doesn't work because we are then trying to
    prove a relation involving _every_ [n] but just a _single_ [m]. *)

(** The successful proof of [double_injective] leaves [m] in the goal
    statement at the point where the [induction] tactic is invoked on
    [n]: *)

Theorem double_injective : forall n m,
     double n = double m ->
     n = m.
Proof.
  elim=> [| n IHn']; first by case.
    case; first by [].

    (* m = S m' *)
    move=> m' eq.
    apply: f_equal.
    apply: IHn'.
      by case: eq.
Qed.

(** What you should take away from all this is that we need to be
    careful about using induction to try to prove something too
    specific: To prove a property of [n] and [m] by induction on [n],
    it is sometimes important to leave [m] generic. *)

(** The proof of this theorem (left as an exercise) illustrates
    the same point: *)

Theorem beq_nat_true : forall n m,
    (n =? m) = true -> n = m.
Proof.
  (* FILL IN HERE *) Admitted.
(** [] *)

(** The strategy of doing fewer [move=>] before an [elim] to obtain a
    more general IH doesn't always work by itself; sometimes some
    _rearrangement_ of quantified variables is needed. Suppose, for
    example, that we wanted to prove [double_injective] by induction
    on [m] instead of [n]. *)

Theorem double_injective_take2_FAILED : forall n m,
     double n = double m ->
     n = m.
Proof.
  move=> n m. elim: m => [| m' IH].
  - (* m = O *) by case: n.
  - (* m = S m' *) case: n IH => [| n' IH].
    + (* n = O *) by [].
    + (* n = S n' *) move=> eq. apply: f_equal.
        (* Stuck again here, just like before. *)
Abort.

(** The problem is that, to do induction on [m], we must first
    introduce [n].  (If we simply say [induction m] without
    introducing anything first, Coq will automatically introduce [n]
    for us!)  *)

(** What can we do about this?  One possibility is to rewrite the
    statement of the lemma so that [m] is quantified before [n].  This
    works, but it's not nice: We don't want to have to twist the
    statements of lemmas to fit the needs of a particular strategy for
    proving them!  Rather we want to state them in the clearest and
    most natural way. *)

(** What we can do instead is to first introduce all the quantified
    variables and then _re-generalize_ one or more of them,
    selectively taking variables out of the context and putting them
    back at the beginning of the goal. The [:] tactical does this... *)

(** ... and, in standard [Ltac], there are several tactics dedicated to this purpose, such as [revert] or [generalize dependendent]. We are not going to discuss the details here. *)

Theorem double_injective_take2 : forall n m,
     double n = double m ->
     n = m.
Proof.
  move=> n m.
  (* [n] and [m] are both in the context *)
  move: m n.
  (* Now [m] and [n] are back in the goal and we can do induction on
     [m] and get a sufficiently general IH. *)
  elim=> [| m' IH].
  - (* m = O *) by case. 
  - (* m = S m' *) case.
    + (* n = O *) by [].
    + (* n = S n' *) move=> n' eq. apply: f_equal.
      apply: IH. by case: eq. Qed.

(** Digression: Let's use [beq_nat_true] to prove a similar
    property of identifiers that we may need later. 
    Caveat: in future, we will rather use strings instead of 
     natural indices as identifiers *)
Inductive id : Type := | Id : nat -> id.
Definition beq_id x y := match x, y with | Id m, Id n => m =? n end.

Theorem beq_id_true : forall x y,
  beq_id x y = true -> x = y.
Proof.
  move=> [m] [n] /= H. by rewrite (beq_nat_true m n H). 
Qed.

(* ================================================================= *)
(** ** Unfolding Definitions *)

(** It sometimes happens that we need to manually unfold a Definition
    so that we can manipulate its right-hand side.  For example, if we
    define... *)

Definition square n := n * n.

(** ... and try to prove a simple fact about [square]... *)

Lemma square_mult : forall n m, square (n * m) = square n * square m.
Proof.
  move=> n m /=.

(** ... we get stuck: [/=] doesn't simplify anything at this point,
    and since we haven't proved any other facts about [square], there
    is nothing we can [apply] or [rewrite] with.

    To make progress, we can manually _unfold_ the definition of
    [square] using [ssreflect]'s rewrite tactic: *)
  
    rewrite /square.

(** There is also the Coq basic tactic [unfold], which basically does
    the same thing, but is less versatile than the extended [rewrite]
    tactic. *)

(** Now we have plenty to work with: both sides of the equality are
    expressions involving multiplication, and we have lots of facts
    about multiplication at our disposal.  In particular, we know that
    it is commutative and associative, and from these facts it is not
    hard to finish the proof. *)

  rewrite -!mult_assoc. (* This is from [Nat] library ... *) 
  rewrite [m*(n*m)]mult_comm.
  by rewrite !mult_assoc.
Qed.

(** At this point, a deeper discussion of unfolding and simplification
    is in order.

    You may already have observed that tacticals like [/=] and [by],
    and the [apply] tactic will often unfold the definitions of
    functions automatically when this allows them to make progress.
    For example, if we define [foo m] to be the constant [5]... *)

Definition foo (x: nat) := 5.

(** then the [by] in the following proof will unfold [foo m] to [(fun x
    => 5) m] and then further simplify this expression to just [5]. *)

Fact silly_fact_1 : forall m, foo m + 1 = foo (m + 1) + 1.
Proof. 
  by [].
Qed.

(** However, this automatic unfolding is rather conservative.  For
    example, if we define a slightly more complicated function
    involving a pattern match... *)

Definition bar x :=
  match x with
  | O => 5
  | S _ => 5
  end.

(** ...then the analogous proof will get stuck: *)

Fact silly_fact_2_FAILED : forall m, bar m + 1 = bar (m + 1) + 1.
Proof.
  Fail by []. (* Does nothing! *)
Abort.

(** 
 -  Why [by] doesn't make progress here? 
 -  After tentatively unfolding [bar m], it is left with a match
    whose scrutinee, [m], is a variable... 
 -  so the [match] cannot be
    simplified further. 
 -  (It is not smart enough to notice that the two
    branches of the [match] are identical.) 
 -  So it gives up on
    unfolding [bar m] and leaves it alone. 
 -  Similarly, tentatively
    unfolding [bar (m+1)] leaves a [match] whose scrutinee is a
    function application (that, itself, cannot be simplified, even
    after unfolding the definition of [+]), so [simpl] leaves it
    alone. *)

(**

    At this point, there are two ways to make progress. One is to use
    [case] to break the proof into two cases, each focusing on a more
    concrete choice of [m] ([O] vs [S _]). In each case, the [match]
    inside of [bar] can now make progress, and the proof is easy to
    complete. *)

Fact silly_fact_2 : forall m, bar m + 1 = bar (m + 1) + 1.
Proof.
  by case.
Qed.

(** This approach works, but it depends on our recognizing that the
    [match] hidden inside [bar] is what was preventing us from making
    progress. *)

(** A more straightforward way to make progress is to explicitly tell
    Coq to unfold [bar]. *)

Fact silly_fact_2' : forall m, bar m + 1 = bar (m + 1) + 1.
Proof.
  move=> m.
  (*rewrite /bar.*)

(** Now it is apparent that we are stuck on the [match] expressions on
    both sides of the [=], and we can use [case] to finish the
    proof without thinking too hard. *)

  by case: m.
Qed.

(* ================================================================= *)
(** ** Destructing Compound Expressions *)

(** The [case] tactic can be used on expressions as well as
    variables: *)

Definition sillyfun (n : nat) : bool :=
  if n =? 3 then false
  else if n =? 5 then false
  else false.

Theorem sillyfun_false : forall (n : nat),
  sillyfun n = false.
Proof.
  move=> n. rewrite /sillyfun.
  case: (n =? 3).
    - (* beq_nat n 3 = true *) by [].
    - (* beq_nat n 3 = false *) case: (n =? 5); by []. Qed.

(* INSTUCTORS: TML19 combine and split exercises moved to HA *)

(** However, [destruct]ing/[case]ing compound expressions requires a bit of care, as
    such [case]s can sometimes provide not enough new information we
    need to complete a proof. *)

Definition sillyfun1 (n : nat) : bool :=
  if n =? 3 then true
  else if n =? 5 then true
  else false.

Theorem sillyfun1_odd_FAILED : forall (n : nat),
     sillyfun1 n = true ->
     odd n = true.
Proof.
  move=> n eq. rewrite /sillyfun1 in eq.
  case: (n =? 3).
  (* stuck... *)
Abort.

(** We can add an identifier before the [:] tactical to save
    this information. *)

(** The [Ltac] way of doing those things is by writing [eqn:]_identifier_ after destructed expression. *)

Theorem sillyfun1_odd : forall (n : nat),
     sillyfun1 n = true ->
     odd n = true.
Proof.
  move=> n eq. rewrite /sillyfun1 in eq.
  case Heqe3: (n =? 3).
    - (* e3 = true *) by rewrite (beq_nat_true _ _  Heqe3).
    - (* e3 = false *)
      case Heqe5: (n =? 5).
        + (* e5 = true *)
          by rewrite (beq_nat_true _ _ Heqe5).
        + (* e5 = false *)
          by rewrite Heqe3 Heqe5 in eq.  Qed.


(* ================================================================= *)
(** ** Review *)

(** We've now seen many of Coq's and [ssreflect]'s most fundamental
    tactics. We'll introduce a few more in the coming chapters, and
    later on we'll see some more powerful _automation_ tactics that
    make Coq help us with low-level details. But basically we've got
    what we need to get work done.

    Here are the ones we've seen:

      - [move=>]: move hypotheses/variables from goal to context

      - [move: x]: move the variable [x] from the context back to an
        explicit hypothesis in the goal formula, if no other
        hypothesis depends on [x]

      - [by []]: finish the proof when it is trivial and reason by
        distinctness of constructors

      - [apply]: prove goal using a hypothesis, lemma, or constructor

      - [... /= ...]: simplify computations

      - [rewrite]: use an equality hypothesis (or lemma) to rewrite
        the goal

      - [... in ...]: rewrite, simplify, etc. in a hypothesis

      - [symmetry]: changes a goal of the form [t=u] into [u=t]

      - [rewrite /...]: replace a defined constant by its right-hand
        side in the goal

      - [case]: case analysis on values of inductively defined types,
        reason by injectivity of constructors

      - [case name:...]: specify the name of an equation to be added to
        the context, recording the result of the case analysis (in Ltac, it is 
          [destruct (...) eqn: name]

      - [elim]: induction on values of inductively defined types

      - [assert (H: e)] (or [assert (e) as H]): introduce a "local
        lemma" [e] and call it [H] *)

(* ================================================================= *)
(** ** Standard Coq alternatives *)

(** Of course, all the tasks we dealt with in
    this chapter can be done without [ssreflect] using basic Coq
    tactics.

    Remember though that the standard Coq tactics
    in many cases implement fragile heuristics for name generation and
    context manipulation, which can make proof scripts a lot harder to
    read and understand.

    For completeness, here is a list of alternative tactics (not)
    covered in this chapter:

      - [apply ... in ...]: "forward reasoning" by applying inside
        hypotheses

      - [destruct]: alternative to [case] for compound expressions;
        substitutes the term to destruct in the goal and the whole
        context (fragile!)

      - [inversion]: alternative to [case] for injective constructors
        and discriminable equations; introduces equations in the
        context, generating names for them automatically (fragile!)

      - [generalize dependent]: alternative to [move:], but moves
        items in the context dependent on the variable to be
        generalised into the goal as well. *)


(* ================================================================= *)
(** ** Micro Sermon *)

(** Mindless proof-hacking is a terrible temptation...

    Try to resist! *)


